⭐ Jeu de cartes Anki pour les mathématiques au lycée
=====================================================

Ce projet a migré définitivement vers `la forge de l'Éducation nationale <https://forge.apps.education.fr/paternaultlouis/anki-math>`__.
